const express = require('express');
const app = express();

// settings
app.set('port', 9999);

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname+'/src/index.html')
});
app.get('/meeting.html', (req, res) => {
  res.sendFile(__dirname+'/src/meeting.html')
});
app.listen(app.get('port'), () => {
  console.log(`Se levantó el servicio en el puerto ${app.get('port')}`)
})